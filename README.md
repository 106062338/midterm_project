# Software Studio 2019 Spring Midterm Project
## Notice

## Topic
* Project Name : [My forum]
* Key functions (add/delete)
    1. [forum]
    2. [對貼文匿名留言]
    3. [新增貼文(在首頁以及單一貼文的頁面都可以)]
    4. [切頁面到我的貼文]
    5. [總版->不分類，分類貼文->選擇要去哪個版]
    
* Other functions (add/delete)
    1. [[針對貼文按讚]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# [作品網址](https://midtermproject-48ff1.firebaseapp.com)

# Components Description : 
1. [user page] : [從旁邊的sidebar進入，可以看到自己PO過的所有貼文]
2. [single post page] : [click 前往貼文 就可以進入只有一篇的頁面]
3. [post list page] : [就是首頁，所有文章，不分類]
4. [differents types of posts] : [點選sidebar上的"分類文章"，就可以進入選擇分類的頁面]
5. [comment under any post] : [先進入單一文章的頁面，再留言]

# Other Functions Description(1~10%) : 
1. [google sign in] : [點選sign in畫面中的google login就會跳出登入的頁面]
2. [chrome notification] : [如果別人在你進入頁面後才對你的文章留言，就會被歸類為新的留言，就會跳出通知]
3. [css animation] : [在新增文章的頁面，左邊的選單在hover前後會有立體的變化]
4. [按讚] : [可以顯示目前有按過讚的人數，如果你已經按過就不能在按一次]

## Security Report (Optional)
進行空白輸入以及html字串的測試，
如果其中一項符合就顯示為invalid input

