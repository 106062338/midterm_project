// post_list page
var user_email = '';
var user_name = '';
var author_key='';
var curComRef='';
var comListener='';
var mode=1;
var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";   
var str_after_content = "</p></div></div>";
function init() {
    listenToNewComment();
    //message();
    switch(mode)
    {
        case 1:
            postListOnly();
        break;
        case 2:
            singlePostOnly();
        break;
        case 3:
            myPostOnly();
        break;
        default:
            postListOnly();
        break;
    }
    //postListOnly();
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            ////////////////display post list
            var post_area = document.getElementById('login_state_page');
            post_area.style.display = "block";
            ////////////////get user name
            var userNameRef = firebase.database().ref('user_name');
            userNameRef.on('child_added', snap=>{
                if(snap.val().email==user.email)
                {
                    user_name = snap.val().user_name;
                    author_key = snap.key; 
                }
            });
            //////////////////grow menu
            user_email = user.email;
            //alert('get!'+user_email);
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function (e) {
                firebase.auth().signOut();
            });


        } else {
            // It won't show any post if not login
            console.log('not log in');
            menu.innerHTML = "<a class='dropdown-item' href='signIn.html'>Login</a><a class='dropdown-item' href='register.html'>Register</a>";
            //menu.innerHTML = "<a class='dropdown-item' href='template.html'>Login</a><a class='dropdown-item' href='temp_reg.html'>Register</a>";
            var post_area = document.getElementById('login_state_page');
            post_area.style.display = "none";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('post_txt');
    post_type = document.getElementById('type');
    post_topic = document.getElementById('topic');
    post_btn.addEventListener('click', function () {
        ////////////////////////////check message valid
        if(checkHtml(post_topic.value)||checkblank(post_topic.value))
        {
            alert('invalid topic')
        }
        else if(checkHtml(post_txt.value)||checkblank(post_txt.value))
        {
            alert('invalid content');
        }
        else
        {
            ///////////////////////////////push to com_list
            //alert('author:'+user_name);
            var postdata = {
                author: user_name,
                email:user_email,
                post:post_txt.value,
                type:post_type.value,
                topic:post_topic.value, 
                likes:0
            };
            
            postsRef.push(postdata);
            if(post_txt.value==="")console.log(post_txt.value);
            //////////////////////push to type list
            // var typeRef = firebase.database().ref('/type_list/' + post_type.value);
            // typeRef.push(postdata);
            /////////////push to users list
            var authorRef = firebase.database().ref('/user_name/' + author_key + '/posts/');
            var userPostData = {
                topic:post_topic.value,
                type:post_type.value,
                post:post_txt.value
            };
            authorRef.push(userPostData);
            ///////////////////restore input box            
            post_txt.value="";
            post_type.value="";
            post_topic.value="";
        }
    });

    // The html code for post


    var postsRef = firebase.database().ref('com_list');
    

    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    postsRef.on('child_added',snap =>{

        var board = document.getElementById("post_list");
        var dataKey = snap.key;
        var likes = snap.val().likes;
        var email = snap.val().email;
        var author = snap.val().author;
        //alert(email);
        var data = snap.val().post;
        //console.log(data);
        var topic = snap.val().topic;
        var type = snap.val().type;
        //console.log(type);
        if(snap.val().email==user_email)
        {
            setMyPostHtml(author, type, topic, data, dataKey, likes);
        }
        switch(type){
            case "sex":
                type = "西斯";
            break;
            case "movie":
                type = "影劇";
            break;
            case "politics":
                type = "政治";
            break;
            case "emotion":
                type = "心情";
            break;
            case "gossip":
                type = "八卦";
            break;
            default:
                type = "不分類";
            break;
        }
        setHtml(author, type, topic, data, dataKey, likes);
    });


}
function setMyPostHtml(author, type, topic, data, dataKey, likes)
{
    myPost = document.getElementById('my_post');
    var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>My Posts</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";   

    switch(type){
        case "sex":
        type = "西斯";
        break;
        case "movie":
        type = "影劇";
        break;
        case "politics":
        type = "政治";
        break;
        case "emotion":
        type = "心情";
        break;
        case "gossip":
        type = "八卦";
        break;
        default:
        type = "不分類";
        break;
    }
    //console.log('author:'+author);
    var new_post = str_bf + author + ' . ' + type + "<span><button onclick='setSinglePostHtml(event)' class='" + dataKey + "'>前往貼文</button></span><span class='d-block text-gray-dark' id='topic'>" + topic + '</span><span class="d-block" id="post_data">'+ data + '</span><span id="' + dataKey + 'mylike">'+likes+' people . like</span><span><button  id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span>'+ str_after_content;
    myPost.innerHTML = myPost.innerHTML + new_post;
}

////////////////////////////switch to user mode
function myPostOnly()
{
    mode=3;
    console.log('my!');
    if(curComRef!='')
        curComRef.off("child_added", comListener);
    board=document.getElementById('post_list');
    single = document.getElementById('post_only');
    myPage = document.getElementById('my_page');

    board.style.display='none';
    single.style.display='none';
    myPage.style.display="block";
}
//////////////////////////////swich to list mode
function postListOnly()
{
    mode=1;
    if(curComRef!='')
        curComRef.off("child_added", comListener);
    board=document.getElementById('post_list');
    single = document.getElementById('post_only');
    myPage = document.getElementById('my_page');

    board.style.display='block';
    single.style.display='none';
    myPage.style.display="none";   
}
//////////////////////////////////////switch to post mode
function singlePostOnly()
{
    mode=2;
    board=document.getElementById('post_list');
    single = document.getElementById('post_only');
    myPage = document.getElementById('my_page');

    board.style.display='none';
    single.style.display='block';
    myPage.style.display="none";  
}
function setPostHtml(author, type, topic, data, dataKey, likes)
{
    board = document.getElementById('post_list');
    //console.log(topic);
    var new_post = str_before_username + author + ' . ' + type + "<span><button onclick='setSinglePostHtml(event)' class='" + dataKey + "'>前往貼文</button></span><span class='d-block text-gray-dark' id='topic'>" + topic + '</span><span class="d-block" id="post_data">'+ data + '</span><span id="'+ dataKey +'listlike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span>'+ str_after_content;
    board.innerHTML = board.innerHTML + new_post;
    //console.log("ready!");
}
async function setHtml(author, type, topic, data, dataKey, likes)
{
    await setPostHtml(author, type, topic, data, dataKey, likes);
    // /////////////////comment update
    //  var commentRef = firebase.database().ref('/com_list/' + dataKey + '/comments');
    //  var curKey = dataKey;
    //  commentRef.on('child_added', function(snapShot){
    //     var commentBoard = document.getElementById('commentBoard_' + dataKey);
    //     //console.log(commentBoard);
    //     var comment = snapShot.val().comment;
    //     // var newComment = '<span>' + comment + '</span><br>';
    //     var newComment = '<p>' + comment + '</p>';
    //     commentBoard.innerHTML = commentBoard.innerHTML + newComment;  
    // });   
}
function pushComment(event)
{
    //alert(event.target.id);
    //console.log(event.target.value);
    if(event.target.value!='')
    {
        var commentRef = firebase.database().ref('/com_list/' + event.target.id + '/comments/');
        //alert(user_email);
        var commentData = {
            //author: user_name,
            email:user_email,
            comment:event.target.value
        };
        commentRef.push(commentData);
            //.then(sendMessageToAuthor(event.target.id));
        //updateComment(event.target);    
        event.target.value="";
    }
    //if(post_txt.value==="")console.log(post_txt.value);
}
function cmtInit(commentRef)
{
    var commentCntInit=0;
    commentRef.once('value')
        .then(snapshot=>{
            ///////to get comment from snap
            // snapshot.forEach(element => {
            //     var data = JSON.stringify(snapshot.val());
            //     var array = data.split('"');
            //     var newComment = array[5];
            // });
            return 1;
        });
    
}
function listenToNewComment()
{
    var postRef = firebase.database().ref('com_list');
    postRef.on('child_added', async(snap)=>{
        var dataKey = snap.key;
        /////author mail
        var author_email = snap.val().email;
        var commentRef = firebase.database().ref('/com_list/'+dataKey+'/comments/');
        var init=0;

        init = await cmtInit(commentRef);
        /////1 for first time
        /////0 for later
        commentRef.on('child_added', snapshot=>{
            ///////////////////only notification for new comment
            /////////no comment or after first load
            var client_email = snapshot.val().email;
            if(author_email===user_email && init===0 && client_email!=user_email)
            {
                sendMessageToAuthor();
            }
         });
         ////////////////back to 0 for later
         init=0;
        
    });
}
function sendMessageToAuthor(dataKey)
{
    message();
    //alert(dataKey);

}
function gotoPostPage(event)
{
    //alert(event.target.className);
    var curPostRef = firebase.database().ref('cur_post');
    var curPost = {
        postKey: event.target.className
    }

    curPostRef.set(curPost);
    window.location.href = "post_page.html";

}
function setSinglePostHtml(event)
{ 
    var dataKey = event.target.className;
    var singlePostRef = firebase.database().ref('/com_list/' + dataKey);
    //curPostRef = postListRef;
    // board = document.getElementById('post_list');
    // board.style.display = 'none';
    singlePostRef.once('value')
    .then( snapshot=>{
        //console.log(snapshot.email);
        var author = snapshot.val().author;
        var email = snapshot.val().email;
        var data = snapshot.val().post;
        var topic = snapshot.val().topic;
        var type = snapshot.val().type;
        var likes = snapshot.val().likes;
        switch(type){
            case "sex":
            type = "西斯";
            break;
            case "movie":
            type = "影劇";
            break;
            case "politics":
            type = "政治";
            break;
            case "emotion":
            type = "心情";
            break;
            case "gossip":
            type = "八卦";
            break;
            default:
            type = "不分類";
            break;
        }
        var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Single Post</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
        var post = str_bf + author + ' . ' + type + "<span class='d-block text-gray-dark' id='topic'>" + topic + "</span><span class='d-block' id='post_data'>"+ data + '</span><span id="'+ dataKey +'singlelike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span><span class="d-block" id="commentBoard"></span><input type="text" class="form-control" placeholder="留言" id="' + dataKey + '" value="" onchange="pushComment(event)">'+ str_after_content;        
        singPostArea = document.getElementById('post_only');
        singPostArea.innerHTML = post;
        /////////////////comment update
            var commentRef = firebase.database().ref('/com_list/' + dataKey + '/comments');
            curComRef = commentRef;

            comListener = commentRef.on('child_added', commentsnap=>{
                var commentBoard = document.getElementById('commentBoard');
                //console.log(commentBoard);
                var newComment = commentsnap.val().comment;
                //console.log(commentBoard);
                commentBoard.innerHTML = commentBoard.innerHTML + '<p>' + newComment + '</p>';
                //console.log(newComment);
                
            });
        
    });
    
    singlePostOnly();

}
function likeClick(event)
{
    //alert(event.target.id);
    var articleKey = event.target.id;
    var postRef = firebase.database().ref('/com_list/' + articleKey);
    var userRef = firebase.database().ref('/user_name/');
    var userLikeRef;
    var alreadyLike = false;
    /////////////////////////////////////////////////////////////go get user like ref
    var listener = userRef.on('child_added', snap=>{
        if(snap.val().user_name==user_name)
        {
            var userKey = snap.key;
            userLikeRef = firebase.database().ref('/user_name/' + userKey + '/likes/');
        }
    });
    userRef.off('child_added', listener);
    //////////////////////////////////////////////check if ant like match this article
    userLikeRef.on('child_added', snap=>{
        if(snap.val().postKey==articleKey)
        {
            alreadyLike = true;
            alert("already like!");
        }
    });
    var likesNum = 0;
    //////////////////////////////////////////////////////like+1 if first time to like
    if(!alreadyLike)
    {
        postRef.once('value')
        .then(snapshot=>{
            var email = snapshot.val().email;
            var data = snapshot.val().post;
            var topic = snapshot.val().topic;
            var type = snapshot.val().type;
            likesNum = snapshot.val().likes + 1;
            console.log(likesNum);
            
            var postdata = {
                author: user_name,
                email: email,
                post:data,
                type:type,
                topic:topic, 
                likes:likesNum
            };
            postRef.set(postdata);
            ///////////////////////////////////////////////////////////////////////time17:00
            switch(mode)
            {
                case 1:
                    ////////////////////////////////////////////////////////////update list html display 
                    likeTxt = document.getElementById(articleKey+'listlike');
                    likeTxt.innerHTML = likesNum + ' people . like';
                    console.log('txt:'+likeTxt.innerHTML);
                break;
                case 2:
                    ////////////////////////////////////////////////////////////update single html display 
                    likeTxt = document.getElementById(articleKey+'singlelike');
                    likeTxt.innerHTML = likesNum + ' people . like'; 
                break;
                case 3:
                    ////////////////////////////////////////////////////////////update list html display 
                    likeTxt = document.getElementById(articleKey+'mylike');
                    likeTxt.innerHTML = likesNum + ' people . like';
                break;
                
            }
             //console.log('fuck');
        });
        //////////////////////////////////////////////////////push to user's like list
        var userLikePostData = {
            postKey: articleKey
        }
        userLikeRef.push(userLikePostData);
    }

}
function goToMy()
{
    window.location.href = "my.html";
}
function message()
{
    var notifyConfig = {
        body: '\\ ^o^ /'
      };
      //Notification.permission = 'granted';
      //console.log('ppp:'+Notification.permission);
      if (Notification.permission === 'default' || Notification.permission === 'undefined'|| Notification.permission === 'denied') {
          console.log('1');
          var notification_1 = new Notification('Hi hh!', notifyConfig);
          Notification.requestPermission().then(function(permission) { 
            console.log('request...');
            if (permission === 'granted') { // 使用者同意授權
                var notification = new Notification('Hia', notifyConfig); // 建立通知
                console.log('2');
            }
            else
            {
                if (Notification.permission === "default") {
                    alert("Notifications blocked. Please enable them in your browser.");
                  }
            }

         });
        
      }
      else
      {
          //console.log(Notification.permission);
          var notification = new Notification('you get a comment!', notifyConfig);
      }

}
function typePost()
{
    window.location.href='type.html';
}
function checkHtml(htmlStr) {
    var  reg = /<[^>]+>/g;
    return reg.test(htmlStr);
}
function checkblank(str)
{
    return str==='';
}
function writePost()
{
    window.location.href = 'new_post.html';
}
window.onload = function () {
    init();
};

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
