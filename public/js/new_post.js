var user_email='';
var user_name='';
function init()
{
    
    /////////////////////////////////////////////////////////get login data
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            ////////////////get user name
            var userNameRef = firebase.database().ref('user_name');
            userNameRef.on('child_added', snap=>{
                if(snap.val().email==user.email)
                {
                    user_name = snap.val().user_name;
                    author_key = snap.key; 
                }
            });
            //////////////////grow menu
            user_email = user.email;
            //alert('get!'+user_email);
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function (e) {
                firebase.auth().signOut();
            });


        } else {
            // It won't show any post if not login
            alert('login first!');
            menu.innerHTML = "<a class='dropdown-item' href='signIn.html'>Login</a><a class='dropdown-item' href='register.html'>Register</a>";
            
        }
    });
    //////////////////////////for new post
    post_btn.addEventListener('click', function () {
        ////////////////////////////check message valid
        if(checkHtml(post_topic.value)||checkblank(post_topic.value))
        {
            alert('invalid topic')
        }
        else if(checkHtml(post_txt.value)||checkblank(post_txt.value))
        {
            alert('invalid content');
        }
        else
        {
            ///////////////////////////////push to com_list
            //alert('author:'+user_name);
            var postdata = {
                author: user_name,
                email:user_email,
                post:post_txt.value,
                type:post_type.value,
                topic:post_topic.value, 
                likes:0
            };
            
            postsRef.push(postdata);
            if(post_txt.value==="")console.log(post_txt.value);
            //////////////////////push to type list
            // var typeRef = firebase.database().ref('/type_list/' + post_type.value);
            // typeRef.push(postdata);
            /////////////push to users list
            var authorRef = firebase.database().ref('/user_name/' + author_key + '/posts/');
            var userPostData = {
                topic:post_topic.value,
                type:post_type.value,
                post:post_txt.value
            };
            authorRef.push(userPostData);
            ///////////////////restore input box            
            post_txt.value="";
            post_type.value="";
            post_topic.value="";
        }
    });
} 
function gotoIndex()
{
    window.location.href='index.html';
}
function checkHtml(htmlStr) {
    var  reg = /<[^>]+>/g;
    return reg.test(htmlStr);
}
function checkblank(str)
{
    return str==='';
}
function typePost()
{
    window.location.href = 'type.html';
}
window.onload = function () {
    init();
};

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
