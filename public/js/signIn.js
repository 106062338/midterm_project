var user_exist=0;
var online_email='';
function initApp() {
    // Login with Email/Password
    var txtUserName = document.getElementById('inputUserName');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        console.log('l');
        const auth = firebase.auth();
        const email = txtEmail.value;
        const password = txtPassword.value;
        const userName = txtUserName.value;
        var userRef = firebase.database().ref('user_name');
        /////////////////////////////////////check message valid
        if(checkHtml(email)||checkblank(email))
        {
            alert('invalid email');
        }
        else if(checkHtml(password)||checkblank(password))
        {
            alert('invalid password');
        }
        else if(checkHtml(userName)||checkblank(userName))
        {
            alert('invalid username');
        }
        else
        {
            /////////////////////////////////////////check user exist
            if(user_exist==1)
            {
                if(online_email==email)
                {
                    console.log(email+"hh");
                    auth.signInWithEmailAndPassword(email, password)
                      .then(e =>{
                        alert(`${userName} had just logged in!!`);
                        window.location.href = "index.html";
                      })
                      .catch(e => alert('wrong password'));
                }
                else
                {
                    console.log(email);
                    alert("wrong email");
                }
            }
            else
            {
                console.log("username does not exist");
            }
        }
        
        
      });
        

    btnGoogle.addEventListener('click', function () {
        console.log('g');
        var provider = new firebase.auth.GoogleAuthProvider();
        console.log(provider);
        firebase.auth().signInWithPopup(provider).then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                //back to index
                window.location.href = "index.html";
                console.log(window.location.href);
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                create_alert("fail", "error");
            });
    });

}
function checkUserExist(event)
{
    user_exist = 0;
    var userRef = firebase.database().ref('user_name');
    userRef.on('child_added', snap =>{
        if(snap.val().user_name==event.target.value)
        {
            user_exist = 1;
            online_email = snap.val().email;
            //alert('user name already exist!');
            console.log(snap.val().email);
            //console.log(userRef);
        } 
    })    
}
function goToRegister()
{
    console.log('r');
    window.location.href = "register.html"; 
    //window.location.href = "temp_reg.html"; 
    console.log(window.location.href);      
}
// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
function checkHtml(htmlStr) {
    var  reg = /<[^>]+>/g;
    return reg.test(htmlStr);
}
function checkblank(str)
{
    return str==='';
}
window.onload = function () {
    initApp();
};