var movie_list;
var sex_list;
var emotion_list;
var politics_list;
var gossip_list;
var singlePost;
var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";   
var str_after_content = "</p></div></div>";
var mode=1;
var user_email='';
var user_name=''
function init()
{
    firebase.auth().onAuthStateChanged(function (user) {
        //console.log(user.email);
        var menu = document.getElementById('dynamic-menu');
        if(user)
        {
            user_email = user.email;
            var userNameRef = firebase.database().ref('user_name');
            userNameRef.on('child_added', snap=>{
                if(snap.val().email==user.email)
                {
                    user_name = snap.val().user_name;
                    author_key = snap.key; 
                }
            });
            //////////////////grow menu
            /////display user
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            ///logout btn
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function (e) {
                firebase.auth().signOut();
            });            
        }
        else
        {
            console.log('not log in');
            menu.innerHTML = "<a class='dropdown-item' href='signIn.html'>Login</a><a class='dropdown-item' href='register.html'>Register</a>";
            //menu.innerHTML = "<a class='dropdown-item' href='template.html'>Login</a><a class='dropdown-item' href='temp_reg.html'>Register</a>";
            var post_area = document.getElementById('login_state_page');
            post_area.style.display = "none";
        }
    });
    var movieBtn = document.getElementById('movieBtn');
    var sexBtn = document.getElementById('sexBtn');
    var emotionBtn = document.getElementById('emotionBtn');
    var politicsBtn = document.getElementById('politicsBtn');
    var gossipBtn = document.getElementById('gossipBtn');
    movie_list = document.getElementById('movie_list');
    sex_list = document.getElementById('sex_list');
    emotion_list = document.getElementById('emotion_list');
    politics_list = document.getElementById('politics_list');
    gossip_list = document.getElementById('gossip_list');
    singlePost = document.getElementById('singlePost');
    hidePost();
    setEachTypeHtml();
    
    movieBtn.addEventListener('click', function () {
        hideList();
        onlyMovie();
    });
    sexBtn.addEventListener('click', function () {
        hideList();
        onlySex();
    });
    emotionBtn.addEventListener('click', function () {
        hideList();
        onlyEmotion();
    });
    politicsBtn.addEventListener('click', function () {
        hideList();
        onlyPolitics();
    });
    gossipBtn.addEventListener('click', function () {
        hideList();
        onlyGossip();
    });

    
}
function setSingleMode()
{
    singlePost.style.display='block';
    movie_list.style.display='none';
    sex_list.style.display='none';
    emotion_list.style.display='none';
    politics_list.style.display='none';
    gossip_list.style.display='none';    
}
function setSinglePostHtml(event)
{
    var dataKey = event.target.className;
    //var type = event.target.id;
    //console.log(type);
    var singlePostRef = firebase.database().ref('/com_list/'+ dataKey);
    //curPostRef = postListRef;
    // board = document.getElementById('post_list');
    // board.style.display = 'none';
    singlePostRef.once('value')
    .then( snapshot=>{
        //console.log(snapshot.email);
        console.log(snapshot.val());
        //alert(type);
        var author = snapshot.val().author;
        var email = snapshot.val().email;
        var data = snapshot.val().post;
        var topic = snapshot.val().topic;
        var type = snapshot.val().type;
        var likes = snapshot.val().likes;
        switch(type){
            case "sex":
            type = "西斯";
            break;
            case "movie":
            type = "影劇";
            break;
            case "politics":
            type = "政治";
            break;
            case "emotion":
            type = "心情";
            break;
            case "gossip":
            type = "八卦";
            break;
            default:
            type = "不分類";
            break;
        }
        var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Single Post</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
        var post = str_bf + author + ' . ' + type + "<span class='d-block text-gray-dark' id='topic'>" + topic + "</span><span class='d-block' id='post_data'>"+ data + '</span><span id="'+ dataKey +'singlelike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span><span class="d-block" id="commentBoard"></span><input type="text" class="form-control" placeholder="留言" id="' + dataKey + '" value="" onchange="pushComment(event)">'+ str_after_content;        
        singPost = document.getElementById('singlePost');
        singPost.innerHTML = post;
        /////////////////comment update
            var commentRef = firebase.database().ref('/com_list/' + dataKey + '/comments');
            curComRef = commentRef;

            comListener = commentRef.on('child_added', commentsnap=>{
                var commentBoard = document.getElementById('commentBoard');
                //console.log(commentBoard);
                var newComment = commentsnap.val().comment;
                //console.log(commentBoard);
                commentBoard.innerHTML = commentBoard.innerHTML + '<p>' + newComment + '</p>';
                //console.log(newComment);
                
            });
        
    });
    
    setSingleMode();

}
function pushComment(event)
{
    //alert(event.target.id);
    //console.log(event.target.value);
    if(event.target.value!='')
    {
        var commentRef = firebase.database().ref('/com_list/' + event.target.id + '/comments/');
        //alert(user_email);
        var commentData = {
            //author: user_name,
            email:user_email,
            comment:event.target.value
        };
        commentRef.push(commentData);
            //.then(sendMessageToAuthor(event.target.id));
        //updateComment(event.target);    
        event.target.value="";
    }
    //if(post_txt.value==="")console.log(post_txt.value);
}
function likeClick(event)
{
    //alert(event.target.id);
    var articleKey = event.target.id;
    var postRef = firebase.database().ref('/com_list/' + articleKey);
    var userRef = firebase.database().ref('/user_name/');
    var userLikeRef;
    var alreadyLike = false;
    /////////////////////////////////////////////////////////////go get user like ref
    var listener = userRef.on('child_added', snap=>{
        if(snap.val().user_name==user_name)
        {
            console.log('u');
            var userKey = snap.key;
            userLikeRef = firebase.database().ref('/user_name/' + userKey + '/likes/');
            console.log(userLikeRef);
        }
    });
    userRef.off('child_added', listener);
    //////////////////////////////////////////////check if ant like match this article
    userLikeRef.on('child_added', snap=>{
        if(snap.val().postKey==articleKey)
        {
            alreadyLike = true;
            alert("already like!");
        }
    });
    var likesNum = 0;
    //////////////////////////////////////////////////////like+1 if first time to like
    if(!alreadyLike)
    {
        postRef.once('value')
        .then(snapshot=>{
            var email = snapshot.val().email;
            var data = snapshot.val().post;
            var topic = snapshot.val().topic;
            var type = snapshot.val().type;
            likesNum = snapshot.val().likes + 1;
            console.log(likesNum);
            
            var postdata = {
                author: user_name,
                email: email,
                post:data,
                type:type,
                topic:topic, 
                likes:likesNum
            };
            postRef.set(postdata);
            ///////////////////////////////////////////////////////////////////////time17:00
            switch(mode)
            {
                case 1:
                    ////////////////////////////////////////////////////////////update list html display 
                    likeTxt = document.getElementById(articleKey+'listlike');
                    likeTxt.innerHTML = likesNum + ' people . like';
                    console.log('txt:'+likeTxt.innerHTML);
                break;
                case 2:
                    ////////////////////////////////////////////////////////////update single html display 
                    likeTxt = document.getElementById(articleKey+'singlelike');
                    likeTxt.innerHTML = likesNum + ' people . like'; 
                break;
                case 3:
                    ////////////////////////////////////////////////////////////update list html display 
                    likeTxt = document.getElementById(articleKey+'mylike');
                    likeTxt.innerHTML = likesNum + ' people . like';
                break;
                
            }
             //console.log('fuck');
        });
        //////////////////////////////////////////////////////push to user's like list
        var userLikePostData = {
            postKey: articleKey
        }
        userLikeRef.push(userLikePostData);
    }

}
function sexHtml(author, type, topic, data, dataKey, likes)
{
    var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Single Post</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
    var new_post = str_before_username + author + ' . ' + type + "<span><button onclick='setSinglePostHtml(event)' class='" + dataKey + "'>前往貼文</button></span><span class='d-block text-gray-dark' id='topic'>" + topic + '</span><span class="d-block" id="post_data">'+ data + '</span><span id="'+ dataKey +'listlike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span>'+ str_after_content;
    sexPost = document.getElementById('sex_list');
    sexPost.innerHTML = sexPost.innerHTML + new_post;
}
function movieHtml(author, type, topic, data, dataKey, likes)
{
    var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Single Post</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
    var new_post = str_before_username + author + ' . ' + type + "<span><button onclick='setSinglePostHtml(event)' class='" + dataKey + "'>前往貼文</button></span><span class='d-block text-gray-dark' id='topic'>" + topic + '</span><span class="d-block" id="post_data">'+ data + '</span><span id="'+ dataKey +'listlike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span>'+ str_after_content;
    moviePost = document.getElementById('movie_list');
    moviePost.innerHTML = moviePost.innerHTML + new_post;
}
function politicsHtml(author, type, topic, data, dataKey, likes)
{
    var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Single Post</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
    var new_post = str_before_username + author + ' . ' + type + "<span><button onclick='setSinglePostHtml(event)' class='" + dataKey + "'>前往貼文</button></span><span class='d-block text-gray-dark' id='topic'>" + topic + '</span><span class="d-block" id="post_data">'+ data + '</span><span id="'+ dataKey +'listlike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span>'+ str_after_content;
    politicsPost = document.getElementById('politics_list');
    politicsPost.innerHTML = politicsPost.innerHTML + new_post;
}
function emotionHtml(author, type, topic, data, dataKey, likes)
{
    var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Single Post</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
    var new_post = str_before_username + author + ' . ' + type + "<span><button onclick='setSinglePostHtml(event)' class='" + dataKey + "'>前往貼文</button></span><span class='d-block text-gray-dark' id='topic'>" + topic + '</span><span class="d-block" id="post_data">'+ data + '</span><span id="'+ dataKey +'listlike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span>'+ str_after_content;
    emotionPost = document.getElementById('emotion_list');
    emotionPost.innerHTML = emotionPost.innerHTML + new_post;
}
function gossipHtml(author, type, topic, data, dataKey, likes)
{
    var str_bf = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Single Post</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
    var new_post = str_before_username + author + ' . ' + type + "<span><button onclick='setSinglePostHtml(event)' class='" + dataKey + "'>前往貼文</button></span><span class='d-block text-gray-dark' id='topic'>" + topic + '</span><span class="d-block" id="post_data">'+ data + '</span><span id="'+ dataKey +'listlike">'+likes+' people . like</span><span><button class="like" id="' + dataKey + '"onclick="likeClick(event)"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i></button></span>'+ str_after_content;
    gossipPost = document.getElementById('gossip_list');
    gossipPost.innerHTML = gossipPost.innerHTML + new_post;
}
function setEachTypeHtml()
{
    postRef = firebase.database().ref('com_list');
    //var moviestr = 'movie';
    postRef.on('child_added', snap=>{
        var dataKey = snap.key;
        var likes = snap.val().likes;
        var email = snap.val().email;
        var author = snap.val().author;
        //alert(email);
        var data = snap.val().post;
        //console.log(data);
        var topic = snap.val().topic;
        var type = snap.val().type;
        
        switch(type){
            case "sex":
                type = "西斯";
                sexHtml(author, type, topic, data, dataKey, likes);
            break;
            case "movie":
                type = "影劇";
                movieHtml(author, type, topic, data, dataKey, likes);
            break;
            case "politics":
                type = "政治";
                politicsHtml(author, type, topic, data, dataKey, likes);
            break;
            case "emotion":
                type = "心情";
                emotionHtml(author, type, topic, data, dataKey, likes);
            break;
            case "gossip":
                type = "八卦";
                gossipHtml(author, type, topic, data, dataKey, likes);
            break;
            default:
                type = "不分類";
            break;
        }
    });
}
function onlyMovie()
{
    singlePost.style.display='none';
    movie_list.style.display='block';
    sex_list.style.display='none';
    emotion_list.style.display='none';
    politics_list.style.display='none';
    gossip_list.style.display='none';
}
function onlySex()
{
    singlePost.style.display='none';
    movie_list.style.display='none';
    sex_list.style.display='block';
    emotion_list.style.display='none';
    politics_list.style.display='none';
    gossip_list.style.display='none';
}
function onlyEmotion()
{
    singlePost.style.display='none';
    movie_list.style.display='none';
    sex_list.style.display='none';
    emotion_list.style.display='block';
    politics_list.style.display='none';
    gossip_list.style.display='none';
}
function onlyPolitics()
{
    singlePost.style.display='none';
    movie_list.style.display='none';
    sex_list.style.display='none';
    emotion_list.style.display='none';
    politics_list.style.display='block';
    gossip_list.style.display='none';
}
function onlyGossip()
{
    singlePost.style.display='none';
    movie_list.style.display='none';
    sex_list.style.display='none';
    emotion_list.style.display='none';
    politics_list.style.display='none';
    gossip_list.style.display='block';
}
function hidePost()
{
    singlePost.style.display='none';
    movie_list.style.display='none';
    sex_list.style.display='none';
    emotion_list.style.display='none';
    politics_list.style.display='none';
    gossip_list.style.display='none';    
}
function hideList()
{
    var list = document.getElementById('type_list');
    list.style.display='none';
}
function gotoIndex()
{
    window.location.href='index.html';
}
function typePost()
{
    window.location.href = 'type.html';
}
function writePost()
{
    window.location.href = 'new_post.html';
}
function gotoIndex()
{
    window.location.href = 'index.html';
}
window.onload = function () {
    init();
};