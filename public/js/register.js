var user_exist=0;
var login=0;
function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var txtUserName = document.getElementById('inputUserName');
    var btnSignUp = document.getElementById('btnSignUp');

    btnSignUp.addEventListener('click', function (e) {
            console.log('c');
            const auth = firebase.auth();
            const email = txtEmail.value;
            const password = txtPassword.value;
            const userName = txtUserName.value;
            /////////////////////////////////////check message valid
            if(checkHtml(email)||checkblank(email))
            {
                alert('invalid email');
            }
            else if(checkHtml(password)||checkblank(password))
            {
                alert('invalid password');
            }
            else if(checkHtml(userName)||checkblank(userName))
            {
                alert('invalid username');
            }            
            ////////////////////if no one use the email and name, then...
            else if(user_exist==0)
            {
                login=1;
                ////////////////create auth user
                auth.createUserWithEmailAndPassword(email, password)
                .then(()=>{
                    // alert(`your account ${email} had just signed up!`);
                    create_alert("success", `${email}`);
                    window.location.href = 'index.html';
                })
                .catch(e=>{
                    create_alert("error", `${email}`);
                    alert('error email');
                    // e => alert(e.message));
                    window.location.href = 'signIn.html';
                    //window.location.href = 'template.html';

                });   
                    /////////////////create user name             
                var userRef = firebase.database().ref('user_name');            
                var userData = {
                    user_name: userName, 
                    email: email
                }
                userRef.push(userData);
            }
            else
            {
                alert("user_name already exist!");
            }    
        
        });
    
}
// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
function checkUserExist(event)
{
    user_exist = 0;
    var userRef = firebase.database().ref('user_name');
    userRef.on('child_added', snap =>{
        if(login==0&&snap.val().user_name==event.target.value)
        {
            user_exist = 1;
            alert("u");
            //return 1;
        }
    })    
}
function checkHtml(htmlStr) {
    var  reg = /<[^>]+>/g;
    return reg.test(htmlStr);
}
function checkblank(str)
{
    return str==='';
}
function signInPage()
{
    window.location.href='signIn.html';
}
window.onload = function () {
    initApp();
};